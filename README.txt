
Education Field is a CCK field for storing information about a person's educational history.

It contains fields for school, state, country, degree, field of study, dates attended, and activities.

Its usefulness is mainly when used as a multiple field, allowing users to submit multiple entries for the various schools they have attended.  It uses javascript (based on link.module) to allow the user to add additional education fields to the node form as needed.

See also the analogous module Employment Field: http://drupal.org/project/employment_field